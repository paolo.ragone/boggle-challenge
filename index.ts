import lineReader, { open } from 'line-reader';
import fs from 'fs';

// const DICTIONARY_PATH = '/usr/share/dict/words';
const DICTIONARY_PATH = './dictionary.txt';
const REMOVE_LOWER_CASE = false;

class Board {
    private coordinates: Coordinate[][] = []; // row(x) -> column(y)
    private flatCoordinates: Coordinate[] = []; // row(x) -> column(y)
    public wordPaths: Path[] = [];
    constructor(public width: number, public height: number, private lettersAsString: string) {
        if (lettersAsString.length != width * height) {
            throw new Error('Board content doesn\'t fit dimensions');
        }
        
        for (var i = 0; i< lettersAsString.length; i=i+width) {
            const row = lettersAsString.substr(i, width);
            this.coordinates[ i / width] = [];
            for (var j = 0; j < width; j++) {
                const c = new Coordinate(j, i / width, row.substr(j, 1).toLowerCase());
                this.coordinates[i / width].push(c);
                this.flatCoordinates.push(c);
            }
        }
        // console.log(this.coordinates);
    }
    isValidCoordinate(x: number,y: number): boolean {
        if (x < 0 || x >= this.width) return false;
        if (y < 0 || y >= this.height) return false;
        return true;
    }
    getCoordinate(x: number, y: number): Coordinate {
        return this.coordinates[y][x];
    }
    findCoordinatesForLetter(letter: string) {
        return this.flatCoordinates.filter(c => c.letter === letter);
    }
    getInitialPathsForLetter(letter: string) {
        return this.findCoordinatesForLetter(letter).map(l => new Path(this, [l]));
    }
    findAdjacentNotIn(toCoordinate: Coordinate, letter: string, path: Path): Coordinate[] {
        const response: Coordinate[] = [];
        if (this.isValidCoordinate(toCoordinate.x - 1, toCoordinate.y)) {
            const coordinate = this.getCoordinate(toCoordinate.x - 1, toCoordinate.y);
            if (coordinate.letter === letter && !path.containsCoordinate(coordinate)) {
                response.push(coordinate);
            }
        }
        if (this.isValidCoordinate(toCoordinate.x + 1, toCoordinate.y)) {
            const coordinate = this.getCoordinate(toCoordinate.x + 1, toCoordinate.y);
            if (coordinate.letter === letter && !path.containsCoordinate(coordinate)) {
                response.push(coordinate);
            }
        }
        if (this.isValidCoordinate(toCoordinate.x, toCoordinate.y - 1)) {
            const coordinate = this.getCoordinate(toCoordinate.x, toCoordinate.y - 1);
            if (coordinate.letter === letter && !path.containsCoordinate(coordinate)) {
                response.push(coordinate);
            }
        }
        if (this.isValidCoordinate(toCoordinate.x, toCoordinate.y + 1)) {
            const coordinate = this.getCoordinate(toCoordinate.x, toCoordinate.y + 1);
            if (coordinate.letter === letter && !path.containsCoordinate(coordinate)) {
                response.push(coordinate);
            }
        }
        return response;
    }
}

class Coordinate {
    constructor(public x: number, public y: number, public letter: string) {}
}

class Path {
    constructor(public board: Board, public coordinates: Coordinate[]) {

    }
    containsCoordinate(coordinate: Coordinate) {
        return this.coordinates.indexOf(coordinate) >= 0;
    }
    findChildPaths(letter: string): Path[] {
        const response: Path[] = [];
        const lastCoordinate = this.coordinates[this.coordinates.length - 1];
        return this.board.findAdjacentNotIn(lastCoordinate, letter, this)
            .map(c => new Path(this.board, [...this.coordinates, c]));
    }
    getWord(): string {
        return this.coordinates.map(c => c.letter).join('');
    }
    markAsWord(): void {
        this.board.wordPaths.push(this);
    }
    toCoordinateString(): string {
        return this.coordinates.map(c => `(${c.x + 1}, ${c.y + 1} - ${c.letter})`).join(' -> ');
    }
}

class DictionaryNode {
    public children = new Map<string, DictionaryNode>();
    public paths: Path[] = [];
    constructor(public letter: string , public isWord: boolean, public parent?: DictionaryNode) {}
    addChild(child: DictionaryNode) {
        this.children.set(child.letter, child);
    }
    getChild(letter: string): DictionaryNode | undefined {
        return this.children.get(letter);
    }
    addPath(path: Path) {
        this.paths.push(path);
    }
}

class Dictionary {
    private topLetters = new Map<string, DictionaryNode>();
    constructor() {}
    load(board: Board) {
        console.log('Starting');
        const startTime = new Date().getTime();    
        var prevWord = 'z'
        var prevWordFound = true;
        var text = fs.readFileSync(DICTIONARY_PATH, 'utf8');
        var textByLine = text.split("\r\n");
        console.log(`Loaded ${textByLine.length} words (${new Date().getTime() - startTime} ms)`);
        var skipped = 0;
        for (const line of textByLine) {
            if (line.length >= 3 && (!REMOVE_LOWER_CASE || line[0] !== line[0].toUpperCase())) {
                // Not an uppercase word with 3 or more letters
                const lcLine = line.toLowerCase();
                if (!lcLine.startsWith(prevWord) || prevWordFound) {
                    // console.log(lcLine);
                    prevWordFound = this.registerLetters(lcLine.substr(1), this.getFirstLetter(board, lcLine[0]));
                } else {
                    // console.log(`Skipping ${lcLine} because ${prevWord} was not found`);
                    skipped++;
                }
                prevWord = lcLine;
            }
        }
        const elapsed = (new Date().getTime()) - startTime;
        console.log(`Done in ${elapsed} ms. Skipped ${skipped} words`);
    }
    getFirstLetter(board: Board, letter: string): DictionaryNode {
        const node = this.topLetters.get(letter);
        if (node) return node;
        const n = new DictionaryNode(letter, false);
        this.topLetters.set(letter, n);
        board.getInitialPathsForLetter(letter).forEach(p => n.addPath(p));
        return n;
    }
    registerLetters(letters: string, parent: DictionaryNode): boolean {
        if (letters.length === 0) throw new Error('Shouldn\'t get here')
        if (parent.paths.length === 0) return false; // Pruned
        const letter = letters[0];
        const node = parent.getChild(letter);
        if (node) {
            return this.registerLetters(letters.substr(1), node);
        } else {
            const newNode = new DictionaryNode(letter, letters.length === 1, parent);
            parent.addChild(newNode);
            parent.paths.forEach(pp => pp.findChildPaths(letter).forEach(p => newNode.addPath(p)));
            if (newNode.isWord && newNode.paths.length > 0 ) {
                newNode.paths.forEach(p => p.markAsWord());
                return true;
            } else if (newNode.paths.length > 0) {
                return this.registerLetters(letters.substr(1), newNode);
            }
            return false;
        }
    } 
}

function doRun() {
    const board = new Board(4,4, "mtnehjdestmnnaes");
    const dictionary = new Dictionary();
    dictionary.load(board);
    return board;
}

function run() {
    const board = doRun();
    const uniqueWords = new Set<string>();
    for (const path of board.wordPaths) {
        console.log(`Found: ${path.getWord()}: ${path.toCoordinateString()}`)
        uniqueWords.add(path.getWord());
    }
    console.log(`Found ${uniqueWords.size} words`)
}

run();