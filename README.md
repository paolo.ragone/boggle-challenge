# Engineering challenge

Taken from: https://wiki.trade.me/display/EN/Solving+a+Boggle+grid

# How to run:
```
npm install
npm start
```

You'll get a result like:

```
Starting
Loaded 279496 words (41 ms)
Done in 181 ms. Skipped 99465 words
Found: ans: (2, 4 - a) -> (1, 4 - n) -> (1, 3 - s)
Found: ats: (2, 4 - a) -> (2, 3 - t) -> (1, 3 - s)
Found: dee: (3, 2 - d) -> (4, 2 - e) -> (4, 1 - e)
Found: deen: (3, 2 - d) -> (4, 2 - e) -> (4, 1 - e) -> (3, 1 - n)
Found: den: (3, 2 - d) -> (4, 2 - e) -> (4, 3 - n)
Found: dens: (3, 2 - d) -> (4, 2 - e) -> (4, 3 - n) -> (4, 4 - s)
Found: dense: (3, 2 - d) -> (4, 2 - e) -> (4, 3 - n) -> (4, 4 - s) -> (3, 4 - e)
Found: ean: (3, 4 - e) -> (2, 4 - a) -> (1, 4 - n)
Found: eans: (3, 4 - e) -> (2, 4 - a) -> (1, 4 - n) -> (1, 3 - s)
Found: eat: (3, 4 - e) -> (2, 4 - a) -> (2, 3 - t)
Found: eats: (3, 4 - e) -> (2, 4 - a) -> (2, 3 - t) -> (1, 3 - s)
Found: een: (4, 1 - e) -> (4, 2 - e) -> (4, 3 - n)
Found: een: (4, 2 - e) -> (4, 1 - e) -> (3, 1 - n)
Found: end: (4, 1 - e) -> (3, 1 - n) -> (3, 2 - d)
Found: ens: (4, 2 - e) -> (4, 3 - n) -> (4, 4 - s)
Found: esne: (3, 4 - e) -> (4, 4 - s) -> (4, 3 - n) -> (4, 2 - e)
Found: mean: (3, 3 - m) -> (3, 4 - e) -> (2, 4 - a) -> (1, 4 - n)
Found: means: (3, 3 - m) -> (3, 4 - e) -> (2, 4 - a) -> (1, 4 - n) -> (1, 3 - s)
Found: meat: (3, 3 - m) -> (3, 4 - e) -> (2, 4 - a) -> (2, 3 - t)
Found: meats: (3, 3 - m) -> (3, 4 - e) -> (2, 4 - a) -> (2, 3 - t) -> (1, 3 - s)
Found: mes: (3, 3 - m) -> (3, 4 - e) -> (4, 4 - s)
Found: mesne: (3, 3 - m) -> (3, 4 - e) -> (4, 4 - s) -> (4, 3 - n) -> (4, 2 - e)
Found: nae: (1, 4 - n) -> (2, 4 - a) -> (3, 4 - e)
Found: naes: (1, 4 - n) -> (2, 4 - a) -> (3, 4 - e) -> (4, 4 - s)
Found: nat: (1, 4 - n) -> (2, 4 - a) -> (2, 3 - t)
Found: nats: (1, 4 - n) -> (2, 4 - a) -> (2, 3 - t) -> (1, 3 - s)
Found: ned: (4, 3 - n) -> (4, 2 - e) -> (3, 2 - d)
Found: nee: (3, 1 - n) -> (4, 1 - e) -> (4, 2 - e)
Found: nee: (4, 3 - n) -> (4, 2 - e) -> (4, 1 - e)
Found: need: (3, 1 - n) -> (4, 1 - e) -> (4, 2 - e) -> (3, 2 - d)
Found: sea: (4, 4 - s) -> (3, 4 - e) -> (2, 4 - a)
Found: sean: (4, 4 - s) -> (3, 4 - e) -> (2, 4 - a) -> (1, 4 - n)
Found: seans: (4, 4 - s) -> (3, 4 - e) -> (2, 4 - a) -> (1, 4 - n) -> (1, 3 - s)
Found: seat: (4, 4 - s) -> (3, 4 - e) -> (2, 4 - a) -> (2, 3 - t)
Found: seats: (4, 4 - s) -> (3, 4 - e) -> (2, 4 - a) -> (2, 3 - t) -> (1, 3 - s)
Found: sned: (4, 4 - s) -> (4, 3 - n) -> (4, 2 - e) -> (3, 2 - d)
Found: snee: (4, 4 - s) -> (4, 3 - n) -> (4, 2 - e) -> (4, 1 - e)
Found: tae: (2, 3 - t) -> (2, 4 - a) -> (3, 4 - e)
Found: taes: (2, 3 - t) -> (2, 4 - a) -> (3, 4 - e) -> (4, 4 - s)
Found: tan: (2, 3 - t) -> (2, 4 - a) -> (1, 4 - n)
Found: tans: (2, 3 - t) -> (2, 4 - a) -> (1, 4 - n) -> (1, 3 - s)
Found 39 words
```
